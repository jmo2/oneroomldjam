﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject Target;
    private Vector3 Offset = Vector3.zero;
    Vector3 OGposition;

	void Start () {
        OGposition = transform.position;
        Offset = transform.position - Target.transform.position;
	}

	
	void Update () {
        try
        {
            if (PlayerScript.player1.isActiveAndEnabled && PlayerScript.player2.isActiveAndEnabled)
            {
                Vector3 Track2 = PlayerScript.player1.transform.position
                + (PlayerScript.player2.transform.position - PlayerScript.player1.transform.position) / 2f;
                transform.position = 0.9f * transform.position + 0.1f * new Vector3(Track2.x, Track2.y, transform.position.z);
            }
            else if(PlayerScript.player1.isActiveAndEnabled)
            {
                transform.position = 0.9f * transform.position + 0.1f * new Vector3(PlayerScript.player1.transform.position.x
                    , PlayerScript.player1.transform.position.y, transform.position.z);
            }
            else if (PlayerScript.player2.isActiveAndEnabled)
            {
                transform.position = 0.9f * transform.position + 0.1f * new Vector3(PlayerScript.player2.transform.position.x
                    , PlayerScript.player2.transform.position.y, transform.position.z);
            }
            else
            {
                transform.position = Target.transform.position + Offset;
            }

        }
        catch
        {
            transform.position = Target.transform.position + Offset;
        }
	}
}
