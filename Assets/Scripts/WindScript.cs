﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindScript : MonoBehaviour {

    [SerializeField]
    private float ZoneLength = 15f;
    [SerializeField]
    private int LengthSubdivision = 30;
    [SerializeField]
    private float ZoneWidth = 2f;
    [SerializeField]
    private int widthSubdivision = 4;
    [SerializeField]
    private float Pressure = 1; // N/m2 
    [SerializeField]
    private float Range = 20;

    [SerializeField]
    private bool UseRandVariance = false;
    [SerializeField]
    private float Variance = 0; // 0 to 100% +- the force;

    public GameObject TrackedObj;

    public Vector3 OriginPoint; // relative to the object

    public Vector3 Direction;

    private void FixedUpdate()
    {
        for(int l = 0; l <= LengthSubdivision; l++)
        {
            for(int w = 0; w <= widthSubdivision; w++)
            {
                RaycastHit hitInfo = new RaycastHit();
                Vector3 perpDir = new Vector3(Direction.y, -Direction.x, Direction.z).normalized * ((float)l * ZoneLength/(float)LengthSubdivision - ZoneLength / 2f); // 90Deg from direction, in xy plane * gridOffset
                Vector3 depthV = Vector3.forward * ((float)w * ZoneWidth / (float)widthSubdivision - ZoneWidth / 2f);
                Vector3 StartPt = TrackedObj.transform.position + OriginPoint + perpDir + depthV ; // particular starting point for this raycast
                bool hitBool = Physics.Raycast(StartPt, Direction, out hitInfo, Range);

                if (hitBool)
                {
                    Rigidbody hitRb = hitInfo.transform.GetComponent<Rigidbody>();
                    float SingleRayForce = Pressure / ((ZoneLength / (float)LengthSubdivision) * (ZoneWidth / (float)widthSubdivision));

                    if(UseRandVariance)
                    {
                        SingleRayForce = SingleRayForce * Random.Range(1 - Variance, 1 + Variance);
                    }

                    hitRb.AddForceAtPosition(Direction.normalized * SingleRayForce, hitInfo.point);

                    Debug.DrawLine(StartPt, hitInfo.point,Color.red);
                }
                else
                {
                    Debug.DrawLine(StartPt, StartPt + Direction.normalized * Range, Color.blue);
                }
            }
        }
    }

}
