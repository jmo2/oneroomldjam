﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisSpawner : MonoBehaviour
{
    public bool running = true;

    [SerializeField]
    GameObject Tracked;
    [SerializeField]
    GameObject debris;
    [SerializeField]
    Vector3 relativeOrigin;
    [SerializeField]
    Vector3 SpawnAreaExtens;

    [SerializeField]
    float rate; // Hertz
    [SerializeField]
    float rateVariance; // Hertz
    [SerializeField]
    bool useRandSize;
    [SerializeField]
    float variance = 2; // anything from 1-inf

    private void Start()
    {
        StartCoroutine(SpawnAThing());
    }

    IEnumerator SpawnAThing()
    {
        if (running)
        {
            GameObject Spawn = Instantiate(debris) as GameObject;
            Vector3 randSizeV = Vector3.one;
            if (useRandSize) {
                //big or small
                if (Random.Range(0, 2) == 1)
                {
                    randSizeV = new Vector3(
                        Spawn.transform.localScale.x * Random.Range(1f, variance),
                        Spawn.transform.localScale.y * Random.Range(1f, variance),
                        Spawn.transform.localScale.z * Random.Range(1f, variance));
                    Spawn.transform.localScale = randSizeV;
                }
                else
                {
                    randSizeV = new Vector3(
                        Spawn.transform.localScale.x * Random.Range(1f / variance, 1f),
                        Spawn.transform.localScale.y * Random.Range(1f / variance, 1f),
                        Spawn.transform.localScale.z * Random.Range(1f / variance, 1f));
                    Spawn.transform.localScale = randSizeV;
                }
            }
            Vector3 rInDefSpace = new Vector3(Random.Range(-SpawnAreaExtens.x, SpawnAreaExtens.x),
                Random.Range(-SpawnAreaExtens.y, SpawnAreaExtens.y),
                Random.Range(-SpawnAreaExtens.z, SpawnAreaExtens.z));
            Spawn.transform.position = Tracked.transform.position + relativeOrigin + rInDefSpace;

            Rigidbody spawnRB = Spawn.GetComponent<Rigidbody>();
            spawnRB.mass = spawnRB.mass * randSizeV.x * randSizeV.y * randSizeV.z;
        }
        yield return new WaitForSeconds(1f / (rate + Random.Range(0f, rateVariance)));
        StartCoroutine(SpawnAThing());
    }
}
