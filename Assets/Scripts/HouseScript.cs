﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HouseScript : MonoBehaviour
{

    [SerializeField]
    private float TerminalV = 10f;
    [SerializeField]
    private DebrisSpawner[] DfromBelow;
    [SerializeField]
    private DebrisSpawner[] DfromAbove;


    [SerializeField]
    GameObject rockyFloor;

    [SerializeField]
    float minFallDist = 100;
    [SerializeField]
    float maxFallDist = 500;
    [SerializeField]
    float minTerrainAngle = 30;
    [SerializeField]
    float maxTerrainAngle = 70;

    [SerializeField]
    int SessionTime = 200; //seconds


    public Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        SpawnGround();
    }

    void SpawnGround()
    {
        GameObject SpawnedTerrain = Instantiate(rockyFloor) as GameObject;
        SpawnedTerrain.transform.position = new Vector3(transform.position.x, transform.position.y - Random.Range(minFallDist, maxFallDist)
            , SpawnedTerrain.transform.position.z);
        SpawnedTerrain.transform.rotation = Quaternion.Euler(0, 0, Random.Range(minTerrainAngle, maxTerrainAngle));
    }

    void Update()
    {
        //test endgame
        if(Input.GetKeyDown(KeyCode.E))
        {
            StartCoroutine(EndGame());
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }

        if (rb.velocity.sqrMagnitude >= TerminalV * TerminalV)
        {
            rb.velocity = rb.velocity.normalized * TerminalV;
        }

        if (Input.GetKey(KeyCode.O))
        {
            transform.Rotate(0, 0, -1);
        }

        if (Input.GetKey(KeyCode.P))
        {

            transform.Rotate(0, 0, 1);
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.transform.tag == "Terrain")
        {
            foreach (DebrisSpawner ds in DfromAbove)
            {
                ds.running = true;
            }
            foreach (DebrisSpawner ds in DfromBelow)
            {
                ds.running = false;
            }
        }
    }
    private void OnTriggerExit(Collider col)
    {
        if (col.transform.tag == "Terrain")
        {
            if (Time.fixedTime > SessionTime)
            {
                Destroy(col.transform.root.gameObject);
                GameObject SpawnedTerrain = Instantiate(rockyFloor) as GameObject;
                SpawnedTerrain.transform.position = new Vector3(transform.position.x, transform.position.y - 30f, SpawnedTerrain.transform.position.z);
                SpawnedTerrain.transform.rotation = Quaternion.Euler(0, 0, 0);
                foreach (DebrisSpawner ds in DfromAbove)
                {
                    ds.running = false;
                }
                foreach (DebrisSpawner ds in DfromBelow)
                {
                    ds.running = false;
                }

                StartCoroutine(EndGame());
            }

            Destroy(col.transform.root.gameObject);
            SpawnGround();
            foreach (DebrisSpawner ds in DfromAbove)
            {
                ds.running = false;
            }
            foreach (DebrisSpawner ds in DfromBelow)
            {
                ds.running = true;
            }
        }
    }

    List<PlayerScript> WhoWon()
    {
        PlayerScript[] players = FindObjectsOfType<PlayerScript>();
        List<PlayerScript> Placing = new List<PlayerScript>();
        Placing.Sort((PlayerScript ps1, PlayerScript ps2) => ps1.Score.CompareTo(ps2.Score));

        
        return Placing;
    }

    IEnumerator EndGame()
    {
        for (int t = 1; t <= 10; t++) {
            print(t.ToString());
            yield return new WaitForSeconds(1);
        }

        if (PlayerScript.player1.Score > PlayerScript.player2.Score)
        {
            PodiumPlayer(PlayerScript.player1, Camera.main.transform.position + new Vector3(-2, 2, 5));
            PodiumPlayer(PlayerScript.player2, Camera.main.transform.position + new Vector3(2, -2, 5));
        }
        else if (PlayerScript.player1.Score < PlayerScript.player2.Score)
        {
            PodiumPlayer(PlayerScript.player2, Camera.main.transform.position + new Vector3(-2, 2, 5));
            PodiumPlayer(PlayerScript.player1, Camera.main.transform.position + new Vector3(2, -2, 5));
        }

        Camera.main.GetComponent<CameraFollow>().enabled = false;


    }

    void PodiumPlayer (PlayerScript ps,Vector3 loc)
    {
        ps.enabled = false;
        ps.rb.isKinematic = true;
        ps.rb.useGravity = false;
        ps.transform.localScale = ps.transform.localScale* 3;
        ps.transform.position = loc;
    }
}


//List<PlayerScript> Placing;
//Placing = WhoWon();
//        for (int i = 0; i<Placing.Count; i++)
//        {
//            print(Placing[i].name);
//Placing[i].enabled = false;
//            Placing[i].rb.isKinematic = true;
//            Placing[i].rb.useGravity = false;
//            Placing[i].transform.localScale = Placing[i].transform.localScale* 3;
//            Placing[i].transform.position = Camera.main.transform.position + new Vector3(4 * (4 - i), 4 * (4 - i), 5);
//        }