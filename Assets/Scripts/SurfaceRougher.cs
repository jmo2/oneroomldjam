﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceRougher : MonoBehaviour {

    Vector3 size;
    [SerializeField]
    GameObject StaticCube;
    [SerializeField]
    int amount = 10;
	void Start () {
        size = transform.lossyScale;

        for (int i = 0; i < amount; i++)
        {
            GameObject spawned = Instantiate(StaticCube) as GameObject;
            spawned.transform.position = transform.position + transform.right * Random.Range(-size.x / 2f, size.x / 2f) + transform.up * Random.Range(0f, size.y / 4f);
            spawned.transform.rotation = Random.rotation;
            spawned.transform.parent = transform.root;
        }
	}
}
