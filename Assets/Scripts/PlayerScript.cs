﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ConstantForce))]

public class PlayerScript : MonoBehaviour
{
    public static PlayerScript player1;
    public static PlayerScript player2;
    private void Awake()
    {
        if (player1 == null)
        { player1 = this;
          PlayerIndex = 1;
        }
        else if (player2 == null)
        { player2 = this;
          PlayerIndex = 2;
        }

        if (PlayerIndex == 1)
        {
            hAxis = "P1Horizontal";
            vAxis = "P1Vertical";
            button1 = "P1Fire1";
            button2 = "P1Fire2";
        }
        else if (PlayerIndex == 2)
        {
            hAxis = "P2Horizontal";
            vAxis = "P2Vertical";
            button1 = "P2Fire1";
            button2 = "P2Fire2";
        }

        string[] joyNames = Input.GetJoystickNames();
        for (int i = 0; i < joyNames.Length; i++)
        {
            print("Index = " + i.ToString() + joyNames[i]);
        }
        StartCoroutine(JsTest());
    }

    public int PlayerIndex = 0;
    public int Score = 0;

    string hAxis = "";
    string vAxis = "";
    string button1 = "";
    string button2 = "";

    [SerializeField]
    float groundSpeed = 1;
    [SerializeField]
    float airSpeed = 0.5f;
    [SerializeField]
    float JumpStrength = 10f;

    [SerializeField]
    float DashLength = 1;
    [SerializeField]
    float DashTime = 1; //seconds spent while dashing   
    [SerializeField]
    float DashDelay = 1;
    [SerializeField]
    float DashHitForce = 10;

    [SerializeField]
    float KillDistance = 15f;

    [SerializeField]
    GameObject TrackedStage;
    Rigidbody trackedRB;

    [SerializeField]
    TextMesh ScoreTMesh;

    [SerializeField]
    float groundClearance = 0.7f;

    public bool LeftWalled = false;
    public bool RightWalled = false;
    public bool Grounded = false;
    public bool Dashing = false;
    public bool DashDamaging = false;



    public Rigidbody rb;
    public ConstantForce cf;

    private void Start()
    {
        Input.GetJoystickNames();


        rb = GetComponent<Rigidbody>();
        cf = GetComponent<ConstantForce>();
        if (TrackedStage.name == null)
        { TrackedStage = null; }
        else { trackedRB = TrackedStage.GetComponent<Rigidbody>(); }
    }

    void ContactCheck(Vector3 Direction, out RaycastHit rh, out bool hitBool)
    {

        hitBool = Physics.Raycast(transform.position, Direction, out rh, 2f);
        Debug.DrawLine(transform.position, Direction + transform.position);
        if (hitBool)
        { hitBool = rh.distance < groundClearance; }
        else { hitBool = false; }
    }

    void FixedUpdate()
    {
        ScoreTMesh.text = Score.ToString();

        RaycastHit leftInfo = new RaycastHit();
        bool leftHit;
        RaycastHit rightInfo = new RaycastHit();
        bool rightHit;
        RaycastHit groundInfo = new RaycastHit();


        ContactCheck(Vector3.left, out leftInfo, out leftHit);
        ContactCheck(Vector3.right, out rightInfo, out rightHit);
        ContactCheck(Vector3.down, out groundInfo, out Grounded);




        if((transform.position - TrackedStage.transform.position).sqrMagnitude >= KillDistance * KillDistance)
        {
            Die();
            Invoke("Revive", 2);
        }

        // rules for jumping
        if (Input.GetButtonDown(button1))
        {
            if ((Grounded || leftHit || rightHit) && !Dashing )
            {
                 rb.velocity += (new  Vector3( Input.GetAxis(hAxis), Input.GetAxis(vAxis),0) 
                        + Vector3.up).normalized * JumpStrength; 
                
            }
           
        }

        //running
        if (Grounded && !Dashing)
        {
            if (Input.GetAxis(hAxis) >= 0.1f)
            {
                rb.position += new Vector3(Input.GetAxis(hAxis), 0, 0) * groundSpeed * Time.deltaTime;
            }
            if (Input.GetAxis(hAxis) <= -0.1f)
            {
                rb.position += new Vector3(Input.GetAxis(hAxis), 0, 0) * groundSpeed * Time.deltaTime;
            }
        }
        else
        {
            if (!Dashing) // floating 
            {
                if (Input.GetAxis(vAxis) <= -0.1)
                {
                    cf.force = new Vector3(0, Input.GetAxis(vAxis) *2, 0) * airSpeed;
                }
                else if (Input.GetAxis(vAxis) >= 0.1)
                {
                    cf.force = new Vector3(0, Input.GetAxis(vAxis), 0) * airSpeed;
                }

                if (Input.GetAxis(hAxis) >= 0.1 || Input.GetAxis(hAxis) <= -0.1)
                {
                    rb.velocity = new Vector3(0, rb.velocity.y, 0);
                }
                rb.position += new Vector3(Input.GetAxis(hAxis), 0, 0) * groundSpeed * Time.deltaTime;
                //rb.position += new Vector3(Input.GetAxis(hAxis),Input.GetAxis(vAxis),0) * airSpeed * Time.deltaTime;
            }

        }

        // set cf to 0 when not using it
        if (Input.GetAxis(hAxis) <= 0.1f && Input.GetAxis(hAxis) >= -0.1f && Input.GetAxis(vAxis) <= 0.1f && Input.GetAxis(vAxis) >= -0.1f)
        {
            cf.force = Vector3.zero;
        }

        if (Input.GetButtonDown(button2) && !Dashing)
        {
            Dashing = true;
            if (TrackedStage != null)
            {
                StartCoroutine(AirDash(new Vector3(Input.GetAxis(hAxis), Input.GetAxis(vAxis), 0), transform.position - TrackedStage.transform.position));
            }
            else
            {
                StartCoroutine(AirDash(new Vector3(Input.GetAxis(hAxis), Input.GetAxis(vAxis), 0), transform.position));
            }
        }
    }

    IEnumerator AirDash(Vector3 Direction, Vector3 OGposition)

    {
        //wait
        for (int wt = 0; wt < 30 * DashDelay; wt++)
        {
            rb.velocity = trackedRB.velocity;
            if (!Dashing)
            {
                break;
            }
            yield return new WaitForSeconds(1f / 30f);
        }

        if (TrackedStage != null)
        {
            

            for (int i = 0; i < 30 * DashTime; i++)
            {
                //check Path
                DashDamaging = true;
                RaycastHit inlineInfo;
                bool inlineHit = Physics.Raycast(transform.position, Direction, out inlineInfo, DashLength * 1 / (30f * DashTime) + 0.5f,1 << 0 ,QueryTriggerInteraction.Ignore);
                Debug.DrawLine(transform.position, TrackedStage.transform.position + OGposition + Direction.normalized * DashLength * (float)i / (30f * DashTime),
                    Color.green, 5f);
                if (!Dashing)
                {
                    DashDamaging = false;
                    break;
                }
                if (inlineHit) // if hit player kill and continue ... else stop
                {
                    if (inlineInfo.transform.tag == "Player")
                    {
                        PlayerScript colPlayer = inlineInfo.transform.GetComponent<PlayerScript>();
                        if (DashDamaging && !colPlayer.DashDamaging)
                        {
                            colPlayer.Die();
                            colPlayer.Invoke("Revive", 2);
                        }
                    }
                    else
                    {
                        if (inlineInfo.transform.tag == "Floor") // smack the floor
                        {
                            Debug.DrawLine(inlineInfo.point, inlineInfo.point + Direction.normalized * DashHitForce, Color.blue, 5);
                            inlineInfo.rigidbody.AddForceAtPosition( Direction.normalized * DashHitForce * inlineInfo.rigidbody.mass , inlineInfo.point);
                        }
                        Dashing = false;

                        rb.velocity = inlineInfo.rigidbody.velocity;
                        transform.position = inlineInfo.point - Direction.normalized * 0.5f;
                        DashDamaging = false;
                        break;
                    }
                }
                rb.velocity = trackedRB.velocity;
                transform.position = TrackedStage.transform.position + OGposition + Direction.normalized * DashLength * (float)i / (30f * DashTime);
                yield return new WaitForSeconds(0);
            }
            Dashing = false;
            DashDamaging = false;
        }
        Dashing = false;
        DashDamaging = false;
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.collider.tag == "Player")
        {
            PlayerScript colPlayer = col.gameObject.GetComponent<PlayerScript>();
            if (DashDamaging && !colPlayer.DashDamaging)
            {
                colPlayer.Die();
                colPlayer.Invoke("Revive", 2);
            }
        }

        if (col.collider.tag == "Floor" && DashDamaging)
        {
            col.rigidbody.AddForceAtPosition(col.contacts[0].normal * DashHitForce * col.rigidbody.mass, col.contacts[0].point);
        }
    }

    public void Die ()
    {
        gameObject.SetActive(false);
        Dashing = false;
    }

    public void Revive()
    {
        rb.velocity = trackedRB.velocity;
        transform.position = trackedRB.position + Vector3.up * 10f;
        gameObject.SetActive(true);
        Dashing = false;
    }

    IEnumerator JsTest()
    {
        while(true)
        {
            if(Input.GetAxis(hAxis) != 0)
            {
                print(hAxis + Input.GetAxis(hAxis).ToString());
            }
            if (Input.GetAxis(vAxis) != 0)
            {
                print(vAxis + Input.GetAxis(vAxis).ToString());
            }
            if (Input.GetButtonDown(button1))
            {
                print(button1 + " Hit");
            }
            if (Input.GetButtonDown(button2))
            {
                print(button2 + " Hit");
            }
            yield return new WaitForSeconds(0);
        }
    }
}
