﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KofTheHillArea : MonoBehaviour {

    List<PlayerScript> ContestingP = new List<PlayerScript>();

    Renderer rend;

    private void Start()
    {
        rend = GetComponent<Renderer>();
    }

    private void FixedUpdate()
    {
        RaycastHit[] InBounds = Physics.BoxCastAll(transform.position, transform.lossyScale / 2f, Vector3.up, transform.rotation, 0.1f);
        ContestingP = new List<PlayerScript>();
        
        foreach(RaycastHit rh in InBounds)
        {
            if(rh.collider.tag == "Player")
            {
                ContestingP.Add(rh.transform.GetComponent<PlayerScript>());
            }
        }
        if(ContestingP.Count == 1)
        {
            ContestingP[0].Score++;
            try
            {
                rend.material.SetColor("_EmissionColor", ContestingP[0].GetComponent<TrailRenderer>().endColor - Color.gray);
            }
            catch { }
        }
        else
        {
            try
            {
                rend.material.SetColor("_EmissionColor", Color.black);
            }
            catch { }
        }
    }
}
