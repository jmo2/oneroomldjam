﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killbox : MonoBehaviour {

    private void OnTriggerExit(Collider col)
    {
        if(col.tag == "Floor")
        {
            Destroy(col.gameObject);
        }
    }
}
